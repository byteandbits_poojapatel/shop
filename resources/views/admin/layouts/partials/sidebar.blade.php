<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        
        <li><a href="{{route('dashboard')}}"><i class="fa fa-book"></i> <span>DashBoard</span></a></li>
        <li class="treeview">
          <a href="#">
          <i class="fa fa-list-alt" aria-hidden="true"></i><span>Category</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('category')}}"><i class="fa fa-circle-o"></i> Add Category</a></li>
            <li><a href="{{route('viewcategory')}}"><i class="fa fa-circle-o"></i> View Category</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
          <i class="ion ion-bag" aria-hidden="true"></i><span>Product</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('product')}}"><i class="fa fa-circle-o"></i> Add Product</a></li>
            <li><a href="{{route('viewproduct')}}"><i class="fa fa-circle-o"></i> View Product</a></li>
          </ul>
        </li>
        <li><a href="{{route('viewcontact')}}"><i class="fa fa-user"></i> <span>Contact</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>