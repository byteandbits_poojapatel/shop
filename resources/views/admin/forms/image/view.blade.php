
  @extends('admin.layouts.app')
@section('content')
  
       <section class="content" style="background-color:white;">
        <!-- Content Header (Page header) -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:30px;">Data of Images</h3>
             
              <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal" style="float:right;">Add New Image</button>
              <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add New Image</button>  -->
            </div><!-- /.box-header --> 

          </div><!-- /.box -->
        <!-- Main content -->
        
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-danger">
                
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Image</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($img as $images)
                      <tr>
                          <td><img src="{{asset('images/product/'.$images->images)}}" height="100" width="100"></td>
                          <td>
                           <a href = "{{ route('deleteimage',['id'=>$images->id])}}"><i class="fa fa-close"></i></button>
                          </td>
                      </tr>
                      @endforeach
                    </tbody>
                    
                  </table>
  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
     
                       <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                        <form  method="POST" action="{{route('addimages',['id'=>$id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title">Image</h4>
                            </div>
                            <div class="modal-body">
                            <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="images">
                                    </div>       
                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default">Submit</button></a>
                            </div>
                        </div>
                        </form>
                        </div>
                    </div>
              
  @endsection
