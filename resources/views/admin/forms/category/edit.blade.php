
@extends('admin.layouts.app')

@section('content')

      <!-- Main content -->
     
        <section class="content" style="background-color:white;">

          <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @foreach($edit as $edits)
            <form role="form" method="POST" action="{{ route('editcategory',['id'=>$edits->id]) }}">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter Category Name here.." value="{{ $edits->name }}">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-danger">Update</button>
              </div>
            </form>
            @endforeach
          </div>
          <!-- /.box -->



        </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     
     
    
    </div><!-- ./wrapper -->
  
    </body>
   
@endsection
  
