<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;


class CartController extends Controller
{
    public function single($id)
    {
       
        $product=Product::where('id','=',$id)->get();
        return view('front/pages/forms/single',compact('product'));
    } 
    public function store($id)
	{
        $product = Product::find($id);
        if($product)
        {
            \Cart::instance('shopping')->add(['id' => $id,
                'name'=>$product->name,$product->sku,1, 
                'price'=>$product->price,
                'qty'=>'1','options' => ['image' => $product->filename]]);
        }
        $carts = \Cart::instance('shopping')->content();
        return \Redirect::route('checkout');
    } 
    public function checkout()
    {
        $cart = \Cart::instance('shopping')->content();
        return view('front/pages/forms/cart',compact('cart'));
    }
    public function destroy($id)
    {
        \Cart::instance('shopping')->remove($id);
        return redirect()->back();  
    }  
}
