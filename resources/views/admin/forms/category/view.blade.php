
  @extends('admin.layouts.app')
@section('content')

       <section class="content" style="background-color:white;">
        <!-- Content Header (Page header) -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:30px;">Data of category</h3>
              <a href="{{route('category')}}"><button class="btn btn-info" style="float:right;">Add Category</button></a>
            </div><!-- /.box-header --> 
          </div><!-- /.box -->

        <!-- Main content -->
        
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-danger">
                
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Category Name</th>
                       
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($category as $categories)
                      <tr>
                          <td>{{ $categories->id }}</td>
                          <td>{{ $categories->name }}</td>
                          
                          <td><a href = "{{ route('editcategory',['id'=>$categories->id])}}"><button class="btn btn-danger"><i class="fa fa-edit"></i></button>
                          </td>
                          <td>
                          <a href = "{{ route('deletecategory',['id'=>$categories->id])}}"><button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                          
                          </td>

                      </tr>
                      @endforeach
                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper --> 
    </div><!-- ./wrapper -->
  
  </body>
 
  @endsection
