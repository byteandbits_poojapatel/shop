<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    public function create()
    {
    return view('admin/forms/category/add');
    }
    public function store(CategoryRequest $request)
    {
      $form=new Category(array(
           'name' => $request->get('name'),
         ));
         
         $form->save();
         $notification = array(
          'message' => 'Your Category Inserted Successfully..', 
          'alert-type' => 'success'
          );
        
          return \Redirect::route('viewcategory')->with($notification);
    }
    public function view()
    {
       $category=Category::all();
       return view('admin.forms.category.view',compact('category'));
    }
    public function edit($id)
    {
        $edit=Category::where('id','=',$id)->get();
        return view('admin.forms.category.edit',compact('edit'));
    }
    public function editstore($id,Request $request)
          {
            \DB::table('categories')
            ->where('id','=',$id)
            ->update(['name'=>$request->get('name') ]);
            $notification = array(
              'message' => 'Your Category Updated Successfully..', 
              'alert-type' => 'success'
              );
            return \Redirect::route('viewcategory')->with($notification);   //Return redirect back on view
           
          }
     public function destroy($id)
    {
            $data=Category::find($id);
            $data->delete();
            $notification = array(
              'message' => 'Your Category Deleted Successfully..', 
              'alert-type' => 'success'
              );
            return  \Redirect::route('viewcategory')->with($notification);
    }



}
