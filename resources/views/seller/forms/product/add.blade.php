
@extends('seller.layouts.app')

@section('content')

 
<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Product</h4>
                            </div>
                            <div class="content">
                            <form role="form" method="POST" action="{{route('sellerproduct')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label>Product Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter Product Name here..">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label>Category Name</label>
                                                
                                                <select class="form-control" name="category_id">
                                                    <option value="">Select a Category</option>
                                                    @foreach ($category as $categories)
                                                    <option value="{{ $categories->id }}">{{ $categories->name }}</option>
                                                    @endforeach
                                                </select>
                                                </div>
                                        </div>
                                       
                                    </div>



                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label>Price</label>
                                            <input type="number" class="form-control" name="price" placeholder="Enter Price here..">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label>Shipping Charge</label>
                                            <input type="number" class="form-control" name="shipping" placeholder="Enter Shipping Charge here..">
                
                                            </div>
                                        </div>
                                       
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>description</label>
                                            <input type="number" class="form-control" name="description" placeholder="Enter Price here..">
                                            </div>
                                            <div class="col-md-12">
                                            <div class="form-group">
                                            <label>Image</label>
                                            <input type="file" class="form-control" name="images[]" multiple="multiple">
                                            </div>
                                        </div>
                                        </div>
                                       
                                       
                                    </div>

                                    <!-- <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                            <label>description</label>
                                                <input type="text" id="editor1" name="description" rows="10" cols="80">
                                                    </div>
                                        </div>
                                    </div> -->

                                    <button type="submit" class="btn btn-danger">Submit</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                   

                </div>
            </div>
        </div>
 
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
@endsection
  
