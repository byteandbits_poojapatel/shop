
  @extends('admin.layouts.app')
@section('content')

       <section class="content" style="background-color:white;">
        <!-- Content Header (Page header) -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:30px;">Data of Contact</h3>
            
            </div><!-- /.box-header --> 
          </div><!-- /.box -->

        <!-- Main content -->
        
          <div class="row">
            <div class="col-xs-12">
             
              <div class="box box-danger">
                
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Message</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($contact as $contacts)
                      <tr>
                          <td>{{ $contacts->id }}</td>
                          <td>{{ $contacts->name }}</td>
                          <td>{{ $contacts->email }}</td>
                          <td>{{ $contacts->contact }}</td>
                          <td>{{ $contacts->message }}</td>
                          <td>
                          <a href="{{route('replycontact',['id'=>$contacts->id])}}"><button class="btn btn-danger"><i class="fa fa-reply"></i></button></a>
                          <a href="{{route('deletecontact',['id'=>$contacts->id])}}"><button class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
                          </td>
                          

                      </tr>
                      @endforeach
                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper --> 
    </div><!-- ./wrapper -->
  
  </body>
 
  @endsection
