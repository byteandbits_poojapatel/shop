<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Contact;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    public function create()
    {
    return view('front/pages/forms/contact');
    }

    public function view()
    {
       $contact=Contact::all();
       return view('admin.forms.contact.view',compact('contact'));
    }
    public function reply($id)
    {
        $contact=Contact::where('id','=',$id)->get();
       return view('admin.forms.contact.reply',compact('contact'));
      
       return redirect()->back();
    }
    public function sendreply(Request $request)
    {
        $data = array('subject'=>$request->get('subject'),'email'=>$request->get('email'),'messages'=>$request->get('message'));
        \Mail::send('admin.forms.contact.send', $data, function($message) use($data)
        {
            $message->to($data['email'])->subject ($data['subject']);
            $message->SetBody($data['messages']);
            $message->from('shopfactory64978@gmail.com','Shop Factory');
        });

        $notification = array(
            'message' => 'Mail Sent successfully.', 
            'alert-type' => 'success'
        );
        return \Redirect::route('viewcontact')->with('notification',$notification);
    }

    // public function sendreply(Request $request)
    // {
        
    //     $data = array('subject'=>$request->get('subject'),'message'=>$request->get('message'),'email'=>$request->get('email'));
    //     \Mail::send('admin.forms.contact.send',$data, function($message) use($data){
    //         $message->to($data['email'])->subject($data['subject']);
    //         $message->from('shopfactory64978@gmail.com','shop');
    //     });

    //     $notification = array(
    //         'message' => 'Mail Sent successfully.', 
    //         'alert-type' => 'success'
    //     );
    //     return redirect()->back()->with($notification);
    // }
    public function store(ContactRequest $request)
    {
      $form=new Contact(array(
           'name' => $request->get('name'),
           'email' => $request->get('email'),
           'contact' => $request->get('contact'),
           'message' => $request->get('message')
         ));
         
         $form->save();
         $notification = array(
          'message' => "Contact Inserted Successfully..", 
          'alert-type' => 'success'
          );
        
          return redirect()->back()->with($notification);
    }
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        $notification = array(
            'message' => 'Your Data Deleted Successfully.',
            'alert-type' => 'success'
        );
        return \Redirect::route('viewcontact')->with($notification);
    }

}
    
