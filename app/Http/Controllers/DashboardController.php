<?php

namespace App\Http\Controllers;
use App\User;
use App\Product;
use App\Category;
use App\Contact;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $product=Product::all();
        $category=Category::all();
        $contact=Contact::all();
    return view('admin/forms/dashboard/index',compact('product','category','contact'));
    }
    public function home()
    {
      $product=  Product::where('category_id','=','2')->get();
      $category=Category::all();
     return view('front/pages/forms/index',compact('product','category'));
    }
    public function about()
    {
     return view('front/pages/forms/about');
    }
    
    public function index()
    {
     return view('user/forms/dashboard/index');
    }
    public function subscribe(Request $request)
    {
        $data = array('emails'=>$request->get('email'));
       \ Mail::send('success', $data, function ($message) use($data)
        {

            $message->from('shopfactory64978@gmail.com', 'Subscribe.');
            $message->SetBody($data['emails']);
            $message->to('shopfactory64978@gmail.com')->subject ('subscribe contact.');

        });
return back();
    }
    
}
