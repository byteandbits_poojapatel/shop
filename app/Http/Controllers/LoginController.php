<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    public function create()
    {
    return view('front/pages/login/login');
    }
    
    public function store(Request $request)
    {

            if(User::loginuser($request->email,$request->password))//Check UserName and Password
            {
            $role=User::where('email','=',$request->email)->pluck('role')->first();
            // return $role;
                    if($role=="1")
                    {
                        $notification = array(
                            'message' => 'login successfully!!.', 
                            'alert-type' => 'success'
                                );
                            return \Redirect::route('dashboard')->with('notification',$notification);
                    }  
                    elseif($role=="2")
                    {
                        $notification = array(
                            'message' => 'login successfully!!.', 
                            'alert-type' => 'success'
                                );
                            return \Redirect::route('userdashboard')->with('notification',$notification);
                    } 
                    elseif($role=="3")
                    {
                        $notification = array(
                            'message' => 'login successfully!!.', 
                            'alert-type' => 'success'
                                );
                            return \Redirect::route('sellerdashboard')->with('notification',$notification);
                    } 
                else
                {
                    $notification = array(
                        'message' => 'wrong email_id or password!!.', 
                        'alert-type' => 'danger'
                            );
                        return redirect()->back()->with('notification',$notification);
                } 

            }
            else
            {
                $notification = array(
                    'message' => 'wrong email_id or password!!.', 
                    'alert-type' => 'danger'
                        );
                return redirect()->back()->with('notification',$notification);
            }
    }
    
    public function logout()
    {
        \Auth::logout();
        return \Redirect::route('login');

    }
}
