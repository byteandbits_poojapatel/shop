<div class="sidebar" data-color="red" data-image="{{asset('seller/assets/img/sidebar-5.jpg')}}">
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="{{route('sellerdashboard')}}" class="simple-text">
                   Seller
                </a>
            </div>

            <ul class="nav">
                <li class="">
                    <a href="{{route('sellerdashboard')}}">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('editprofile')}}">
                        <i class="pe-7s-user"></i>
                        <p>Edit Profile</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('sellerproduct')}}">
                        <i class="pe-7s-news-paper"></i>
                        <p>Product</p>
                    </a>
                    </li>
                    <li>
                    <a href="{{route('view')}}">
                        <i class="pe-7s-user"></i>
                        <p>view product</p>
                    </a>
                </li>
                <!-- </li>
                <li>
                    <a href="table.html">
                        <i class="pe-7s-note2"></i>
                        <p>Table List</p>
                    </a>
                </li>
                <li>
                    <a href="icons.html">
                        <i class="pe-7s-science"></i>
                        <p>Icons</p>
                    </a>
                </li>
                <li>
                    <a href="maps.html">
                        <i class="pe-7s-map-marker"></i>
                        <p>Maps</p>
                    </a>
                </li>
                <li>
                    <a href="notifications.html">
                        <i class="pe-7s-bell"></i>
                        <p>Notifications</p>
                    </a>
                </li> -->
				
            </ul>
    	</div>
    </div>