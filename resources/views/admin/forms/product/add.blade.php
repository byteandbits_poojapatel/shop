
@extends('admin.layouts.app')

@section('content')

 
      <!-- Main content -->
     
        <section class="content" style="background-color:white;">

          <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Product</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{route('product')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label>Product Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter Product Name here..">
                </div>
                <div class="form-group">
                    <label>Category Name</label>
                    
                    <select class="form-control" name="category_id">
                        <option value="">Select a Category</option>
                        @foreach ($category as $categories)
                        <option value="{{ $categories->id }}">{{ $categories->name }}</option>
                        @endforeach
                    </select>
                 </div>
                <div class="form-group">
                  <label>Price</label>
                  <input type="number" class="form-control" name="price" placeholder="Enter Price here..">
                </div>
                <div class="form-group">
                  <label>description</label>
                  <!-- <input type="textarea" class="form-control" name="description" placeholder="Enter description here.."> -->
                  <textarea id="editor1" name="description" rows="10" cols="80">
                                          
                    </textarea>
                  
                </div>
                <div class="form-group">
                  <label>Shipping Charge</label>
                  <input type="number" class="form-control" name="shipping" placeholder="Enter Shipping Charge here..">
                </div>
               
                <div class="form-group">
                  <label>Image</label>
                  <input type="file" class="form-control" name="images[]" multiple="multiple">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-danger">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->



        </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
   </div>
    </div><!-- ./wrapper -->
   
    </body>
 
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
@endsection
  
