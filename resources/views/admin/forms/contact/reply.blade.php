
@extends('admin.layouts.app')

@section('content')
        <section class="content" style="background-color:white;">

          <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Category</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @foreach($contact as $contact)
            <form role="form" method="POST" action="{{route('replycontact',['id'=>$contact->id])}}">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                <label> Email</label>
                
                <input type="name" class="form-control" name=email placeholder="Email" value="{{$contact->email}}">
                </div>
                <div class="form-group">
                <label>Subject</label>
                <input type="name" class="form-control" name=subject placeholder="Subject" value="" required>
                </div>
                <div class="form-group">
                <label>Message</label>
                <textarea id="editor1" name="message" rows="10" cols="80" required></textarea>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-danger">Submit</button>
              </div>
            </form>
          @endforeach
          </div>
          <!-- /.box -->



        </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
     
     
    
    </div>
    <!-- ./wrapper -->
    <script>
  $(function () {
        CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
    
@endsection
  
