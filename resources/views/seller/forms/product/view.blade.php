
  @extends('seller.layouts.app')
@section('content')

       <section class="content">
        <!-- Content Header (Page header) -->
        <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:30px;">Data of Products</h3>
              <a href="{{route('sellerproduct')}}"><button class="btn btn-info" style="float:right;">Add Product</button></a>
            </div><!-- /.box-header --> 
          </div><!-- /.box -->

        <!-- Main content -->
        
          <div class="row" >
            <div class="col-xs-12">
             
              <div class="box box-danger">
                
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped" style="overflow-x: auto;">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Product Name</th>
                        <th>category Id</th>
                        <th>price</th>
                        <th>Description</th>
                        <th>Shipping</th>
                        <th>Image</th>
                       
                        <th>Action image</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($product as $products)
                      <tr>
                          <td>{{ $products->id }}</td>
                          <td>{{ $products->name }}</td>
                          <td>{{ $products->category_id }}</td>
                          <td>{{ $products->price }}</td>
                          <td>{{ $products->description }}</td>
                          <td>{{ $products->shipping }}</td>
                          <td><img src="{{asset('images/product/'.$products->filename)}}" height="100" width="100"></td>
                          <td>
                          <a href = "{{ route('sellerviewimage',['id'=>$products->id])}}"><button class="btn btn-success"><i class="fa fa-picture-o"></i></button>
                          </td>
                          <td>
                          <a href = ""><button class="btn btn-danger"><i class="fa fa-edit"></i></button>
                          </td>
                          <td>
                          <a href = "{{ route('delete',['id'=>$products->id])}}"><button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                         
                          </td>

                      </tr>
                      @endforeach
                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
  @endsection
