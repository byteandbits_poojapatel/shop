<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'admin'], function()
{
Route::get('dashboard',array('as'=>'dashboard','uses'=>'DashboardController@dashboard')); 
Route::get('category',array('as'=>'category','uses'=>'CategoryController@create'));
Route::post('category',array('as'=>'category','uses'=>'CategoryController@store'));
Route::get('category/view',array('as'=>'viewcategory','uses'=>'CategoryController@view'));
Route::get('category/delete/{id}',array('as'=>'deletecategory','uses'=>'CategoryController@destroy'));  
Route::get('category/edit/{id}',array('as'=>'editcategory','uses'=>'CategoryController@edit'));
Route::post('category/edit/{id}',array('as'=>'editcategory','uses'=>'CategoryController@editstore'));


Route::get('product',array('as'=>'product','uses'=>'ProductController@create'));
Route::post('product',array('as'=>'product','uses'=>'ProductController@store'));
Route::get('product/view',array('as'=>'viewproduct','uses'=>'ProductController@view'));
Route::get('product/delete/{id}',array('as'=>'deleteproduct','uses'=>'ProductController@destroy'));  
Route::get('product/edit/{id}',array('as'=>'editproduct','uses'=>'ProductController@edit'));
Route::post('product/edit/{id}',array('as'=>'editproduct','uses'=>'ProductController@editstore'));

Route::get('image/view/{id}',array('as'=>'viewimage','uses'=>'ProductController@show'));
Route::get('image/delete/{id}',array('as'=>'deleteimage','uses'=>'ProductController@deleteimage'));
Route::post('image/add/{id}',array('as'=>'addimages','uses'=>'ProductController@addimage'));

Route::get('contact/view',array('as'=>'viewcontact','uses'=>'ContactController@view'));
Route::get('contact/reply/{id}',array('as'=>'replycontact','uses'=>'ContactController@reply'));
Route::post('contact/reply/{id}',array('as'=>'replycontact','uses'=>'ContactController@sendreply'));
Route::get('contact/delete/{id}', array('as'=>'deletecontact','uses'=>'ContactController@destroy'));

}
);

Route::get('home',array('as'=>'home','uses'=>'DashboardController@home'));
Route::post('subscribe',array('as'=>'subscribe','uses'=>'DashboardController@subscribe'));
Route::get('userdashboard',array('as'=>'userdashboard','uses'=>'DashboardController@index'));
Route::get('about',array('as'=>'about','uses'=>'DashboardController@about'));
Route::get('single/{product}',array('as'=>'single','uses'=>'CartController@single'));
Route::get('contact',array('as'=>'contact','uses'=>'ContactController@create'));
Route::post('contact',array('as'=>'contact','uses'=>'ContactController@store'));
//Seller
Route::get('sellerdashboard',array('as'=>'sellerdashboard','uses'=>'SellerController@create'));
Route::get('profile',array('as'=>'profile','uses'=>'SellerController@profile'));
Route::get('sellerproduct',array('as'=>'sellerproduct','uses'=>'SellerController@product'));
Route::post('sellerproduct',array('as'=>'sellerproduct','uses'=>'SellerController@store'));
Route::get('editprofile',array('as'=>'editprofile','uses'=>'SellerController@editprofile'));
Route::get('view',array('as'=>'view','uses'=>'SellerController@view'));
Route::get('delete/{id}',array('as'=>'delete','uses'=>'SellerController@destroy')); 
Route::get('view/{id}',array('as'=>'sellerviewimage','uses'=>'SellerController@show'));
Route::get('img/delete/{id}',array('as'=>'sellerdeleteimage','uses'=>'SellerController@deleteimage'));
Route::post('imgadd/{id}',array('as'=>'addimg','uses'=>'SellerController@addimg'));

//Cart
Route::get('add-cart/{product}',array('as'=>'addcart','uses'=>'CartController@store')); 
Route::get('deletecart/{id}', array('as'=>'deletecart','uses'=>'CartController@destroy'));
Route::get('checkout',array('as'=>'checkout','uses'=>'CartController@checkout')); 


Route::get('login',array('as'=>'login','uses'=>'LoginController@create'));
Route::post('login',array('as'=>'login','uses'=>'LoginController@store'));
Route::get('registration',array('as'=>'registration','uses'=>'RegistrationController@create'));
Route::post('registration',array('as'=>'registration','uses'=>'RegistrationController@store'));
Route::get('sellerregister',array('as'=>'sellerregister','uses'=>'RegistrationController@index'));
Route::post('sellerregister',array('as'=>'sellerregister','uses'=>'RegistrationController@insert'));
Route::get('logout',array('as'=>'logout','uses'=>'LoginController@logout'));

//Payment Form
Route::get('paywithrazorpay', 'RazorpayController@payWithRazorpay')->name('paywithrazorpay');
Route::post('payment', 'RazorpayController@payment')->name('payment');