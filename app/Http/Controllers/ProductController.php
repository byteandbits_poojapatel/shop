<?php

namespace App\Http\Controllers;
use App\User;
use App\Product;
use App\Category;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Product_image;

class ProductController extends Controller
{
    public function create()
    {
     $category=Category::all();
    return view('admin/forms/product/add',compact('category'));
    }
    public function show($id)
    {
      
    $img=Product_image::where('product_id','=',$id)->get();
    return view('admin/forms/image/view',compact('img','id'));
    }

    public function view()
    {
       $product=Product::all();
       return view('admin.forms.product.view',compact('product','img'));
    }

    public function store(ProductRequest $request)
    {

      $product=new Product(array(
           'name' => $request->get('name'),
           'category_id' => $request->get('category_id'),
           'price' => $request->get('price'),
           'description' => $request->get('description'),
           'shipping' => $request->get('shipping'),
           'filename' => '-'
         ));
         $product->save();
         $p_id=$product->id;

         $pimage=\Input::file('images');
         foreach($pimage as $files)
         {
          $picName = str_random(30).'.'.$files->getClientOriginalExtension(); //Insert path in database
          $files->move(base_path().'/public/images/product/',$picName);       //Insert image in folder
                     $pimage = new Product_image(array(
                         'product_id' => $p_id,
                         'images' => $picName
                     ));
                     $pimage->save();
         }

         \DB::table('products')
          ->where('id','=',$p_id)
          ->update(['filename'=>$picName]);
        
         $notification = array(
          'message' => 'Your Product Inserted Successfully..', 
          'alert-type' => 'success'
          );
          return \Redirect::route('viewproduct')->with($notification);
    }

    public function addimage($id,Request $request)
    {
      $pimage=\Input::file('images');
      $picName = str_random(30).'.'.$pimage->getClientOriginalExtension(); //Insert path in database
      $pimage->move(base_path().'/public/images/product/',$picName);  

      $product=new Product_image(array(
        'product_id'=>$id,
        'images' => $picName,
      ));
      $product->save();
      return redirect()->back();
    }

    public function edit($id)
    {
        $edit=Product::where('id','=',$id)->get();
        return view('admin.forms.product.edit',compact('edit'));
    }
    public function editstore($id,ProductRequest $request)
          {
            \DB::table('products')
            ->where('id','=',$id)
            ->update([
              'name'=>$request->get('name') ,
              'category_id'=>$request->get('category_id'),
              'price'=>$request->get('price'),
              'description'=>$request->get('description'),
              'shipping'=>$request->get('shipping'),
              'filename'=>$request->get('filename')
            ]);
            $notification = array(
              'message' => 'Your Product Updated Successfully..', 
              'alert-type' => 'success'
              );
            return \Redirect::route('viewproduct')->with($notification);   //Return redirect back on view
           
          }
    public function destroy($id)
    {
            $data=Product::find($id);
            $data->delete();
            $notification = array(
              'message' => 'Your Product Deleted Successfully..', 
              'alert-type' => 'success'
              );
            return  \Redirect::route('viewproduct')->with($notification);
    }
    public function deleteimage($id)
    {
            $data=Product_image::find($id);
            $data->delete();
            $notification = array(
              'message' => 'Product Image Deleted Successfully..', 
              'alert-type' => 'success'
              );
            return redirect()->back()->with($notification);
    }
}
