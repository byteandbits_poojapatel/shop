
@extends('admin.layouts.app')

@section('content')

     
        <section class="content" style="background-color:white;">

          <div class="row">
          <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Product</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            @foreach($edit as $edits)
            <form role="form" method="POST" action="{{route('editproduct',['id'=>$edits->id] )}}">
            {{ csrf_field() }}
              <div class="box-body">
                <div class="form-group">
                  <label>Product Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter Product Name here.." value="{{ $edits->name}}">
                </div>
                <div class="form-group">
                    <label>Category Name</label>
                    <input type="text" class="form-control" name="category_id" placeholder="Enter Price here.." value="{{ $edits->category_id}}">
                   
                 </div>
                <div class="form-group">
                  <label>Price</label>
                  <input type="number" class="form-control" name="price" placeholder="Enter Price here.." value="{{ $edits->price}}">
                </div>
                <div class="form-group">
                  <label>description</label>
                  <input type="textarea" class="form-control" name="description" placeholder="Enter description here.." value="{{ $edits->description}}">
                </div>
                <div class="form-group">
                  <label>Shipping Charge</label>
                  <input type="number" class="form-control" name="shipping" placeholder="Enter Shipping Charge here.." value="{{ $edits->shipping}}">
                </div>
               
                <div class="form-group">
                  <label>Image</label>
                  <input type="file" class="form-control" name="filename" placeholder="Enter Category Name here.." value="{{ $edits->filename}}">
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-danger">Update</button>
              </div>
            </form>
            @endforeach
          </div>
          <!-- /.box -->



        </div>
          </div><!-- /.row -->

        </section><!-- /.content -->
     
@endsection
  
