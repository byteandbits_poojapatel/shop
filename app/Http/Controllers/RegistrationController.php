<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('front/pages/login/registration');
    }
    public function index()
    {
        return view('front/pages/login/sellerregister');
    }

    public function store(RegisterRequest $request)
    {
        $reg=new User(array(
           'name'=> $request->get('name'),
           'email'=> $request->get('email'),
           'password'=>bcrypt($request->get('password')),
           'role'=>'2'
        ));
        $reg->save();
        return \Redirect::route('home')->with('message', 'Registration Sucessfully done.'); 
    }
    public function insert(RegisterRequest $request)
    {
        $reg=new User(array(
           'name'=> $request->get('name'),
           'email'=> $request->get('email'),
           'password'=>bcrypt($request->get('password')),
           'role'=>'3'
        ));
        $reg->save();
        return \Redirect::route('home')->with('message', 'Registration Sucessfully done.'); 
    }
}
