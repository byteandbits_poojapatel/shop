@extends('seller.layouts.app')

@section('content')
  
<div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Images</h4>
                                 <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#myModal" style="float:right;">Add New Image</button>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>Image</th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    @foreach ($img as $images)
                                    <tr>
                                        <td><img src="{{asset('images/product/'.$images->images)}}" height="100" width="100"></td>
                                        <td>
                                        <a href = "{{ route('sellerdeleteimage',['id'=>$images->id])}}"><i class="fa fa-close"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                      
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                        <form  method="POST" action="{{route('addimg',['id'=>$id])}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                            <h4 class="modal-title">Image</h4>
                            </div>
                            <div class="modal-body">
                            <div class="form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="images">
                                    </div>       
                            </div>
                            <div class="modal-footer">
                            <button type="submit" class="btn btn-default">Submit</button></a>
                            </div>
                        </div>
                        </form>
                        </div>
                    </div>

  @endsection
