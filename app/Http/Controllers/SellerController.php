<?php

namespace App\Http\Controllers;
use App\Product;
use App\Category;
use App\Product_image;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    public function create()
    {
     return view('seller/forms/dashboard/index');
    }
    public function profile()
    {
     return view('seller/forms/dashboard/profile');
    }
    public function editprofile()
    {
     return view('seller/forms/dashboard/editprofile');
    }
    public function view()
    {
       $product=Product::all();
       return view('seller.forms.product.view',compact('product','img'));
    }
    public function product()
    {
     $category=Category::all();
     return view('seller/forms/product/add',compact('category'));
    }
    public function show($id)
    {
      
    $img=Product_image::where('product_id','=',$id)->get();
    return view('seller/forms/image/view',compact('img','id'));
    }
    public function store(ProductRequest $request)
    {

      $product=new Product(array(
           'name' => $request->get('name'),
           'category_id' => $request->get('category_id'),
           'price' => $request->get('price'),
           'description' => $request->get('description'),
           'shipping' => $request->get('shipping'),
           'filename' => '-'
         ));
         $product->save();
         $p_id=$product->id;

         $pimage=\Input::file('images');
         foreach($pimage as $files)
         {
          $picName = str_random(30).'.'.$files->getClientOriginalExtension(); //Insert path in database
          $files->move(base_path().'/public/images/product/',$picName);       //Insert image in folder
                     $pimage = new Product_image(array(
                         'product_id' => $p_id,
                         'images' => $picName
                     ));
                     $pimage->save();
         }

         \DB::table('products')
          ->where('id','=',$p_id)
          ->update(['filename'=>$picName]);
        
         $notification = array(
          'message' => 'Your Product Inserted Successfully..', 
          'alert-type' => 'success'
          );
          return back();
    }
    public function addimage($id,Request $request)
    {
      $pimage=\Input::file('images');
      $picName = str_random(30).'.'.$pimage->getClientOriginalExtension(); //Insert path in database
      $pimage->move(base_path().'/public/images/product/',$picName);  

      $product=new Product_image(array(
        'product_id'=>$id,
        'images' => $picName,
      ));
      $product->save();
      return redirect()->back();
    }
    public function destroy($id)
    {
            $data=Product::find($id);
            $data->delete();
            $notification = array(
              'message' => 'Your Product Deleted Successfully..', 
              'alert-type' => 'success'
              );
            return  \Redirect::route('view')->with($notification);
    }
    public function deleteimage($id)
    {
            $data=Product_image::find($id);
            $data->delete();
            $notification = array(
              'message' => 'Product Image Deleted Successfully..', 
              'alert-type' => 'success'
              );
            return redirect()->back()->with($notification);
    }
}
