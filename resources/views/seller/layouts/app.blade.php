<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="{{ asset('seller/assets/img/favicon.ico')}}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Seller</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('seller/assets/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ asset('seller/assets/css/animate.min.css')}}" rel="stylesheet"/>

    <link href="{{ asset('seller/assets/css/light-bootstrap-dashboard.css')}}" rel="stylesheet"/>



    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('seller/assets/css/demo.css')}}" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('seller/assets/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
    @yield('styles')
    

</head>
<body>

<div class="wrapper">
<div class="main-panel">

@include('seller.layouts.partials.header')
@include('seller.layouts.partials.sidebar')
@yield('content')
@include('seller.layouts.partials.footer')

</div>
</div>

</body>

    <!--   Core JS Files   -->
    <script src="{{ asset('seller/assets/js/jquery.3.2.1.min.js')}}" type="text/javascript"></script>
	<script src="{{ asset('seller/assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

	<!--  Charts Plugin -->
	<script src="{{ asset('seller/assets/js/chartist.min.js')}}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{ asset('seller/assets/js/bootstrap-notify.js')}}"></script>

    

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{ asset('seller/assets/js/light-bootstrap-dashboard.js?v=1.4.0')}}"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="{{ asset('seller/assets/js/demo.js')}}"></script>
    <script src="{{ asset('seller/assets/ckeditor/ckeditor.js')}}"></script>

	

</html>
